const PORT = 7777;
const dgram = require('dgram');
const uuidv4 = require('uuid/v4');
const net = require('net');
const fs = require('fs');

module.exports = class Debugger {
	constructor() {
		this.running = false;
		this.socket = null;
		this.connected = false;
	}


	/**
	 * gets the IPV4 BroadcastAddresses and returns the first one
	 * @returns {String} address
	 */
	getV4BroadcastAddress() {
		const ifaces = require('os').networkInterfaces();
		const iterator = Object.keys(ifaces);
		for (let i = 0; i < iterator.length; i++) {
			let iface = ifaces[iterator[i]];
			for (let j = 0; j < iface.length; j++) {
				if (iface[j].family == "IPv4" && !iface[j].internal) {
					return require('ip').cidrSubnet(iface[j].cidr).broadcastAddress;
				}
			}
		}
	}

	/**
	 * logs AnnouncementMessages via Ipv4
	 */
	logAnnounce4() {
		const server = dgram.createSocket('udp4');
		server.bind(PORT, this.getV4BroadcastAddress());
		server.on('listening', () => server.setBroadcast(true));
	}

	/**
	 * logs AnnouncementMessages via Ipv6
	 */
	logAnnounce6() {
		const server = dgram.createSocket('udp6');
		server.bind(PORT, () => server.addMembership("ff02::1"));
	}

	/**
	 * logs every incomming Announce packet
	 * @param {dgram.Socket} server 
	 */
	logAnnounce(server) {
		server.on('error', console.log);
		const announcementIds = [];

		server.on('message', (msg) => {
			if (msg.command == "announce") {
				if (!(msg.command.server.id in announcementIds)) {
					console.log(msg);
					announcementIds.push(msg.command.server.id);
				}
			}
		});

		process.on('SIGINT', () => {
			server.close();
			process.exit(0);
		});
	}

	/**
	 * logs every incoming ChatMessage.
	 */
	logChat() {
		if (this.socket && this.connected) {
			this.socket.on('data', (data) => {
				let msg = JSON.parse(data.toString('utf8'));
				if (msg.command && msg.command.name && msg.command.name == "chat") {
					console.log(msg.command.from.name + ": " + msg.command.message);
				}
			});
		}
	}

	/**
	 * sends a chat Message.
	 * @param {String} message Message to send
	 * @param {String} name  Name who sent it
	 * @param {Number} color Color of the sender
	 */
	sendChatMessage(message, name, color) {
		this.sendLn(JSON.stringify({
			command: {
				name: "chat",
				message: message,
				from: {
					name: name,
					color: color,
				},
			}
		}));
	}

	/**
	 * announces amount fake servers
	 * @param {Number} amount 
	 */
	announce(amount) {
		let messages = [];
		for (let i = 0; i < amount; i++) {
			messages.push(this.generateAnnounceMessages());
		}

		let server4 = dgram.createSocket('udp4');
		server4.bind("65190"); //bind to random port to say broadcast(true)
		server4.on('listening', () => server4.setBroadcast(true));
		let server6 = dgram.createSocket('udp6');

		server4.on('error', console.log);
		server6.on('error', console.log);

		this.running = true;

		process.on('SIGINT', () => {
			this.running = false;
			server4.close();
			server6.close();
			process.exit(0);
		});

		for (let i = 0; i < messages.length; i++) {
			this.announceMessage(server4, PORT, this.getV4BroadcastAddress(), messages[i]);
			this.announceMessage(server6, PORT - 1, "ff02::1", messages[i]);
		}
	}

	/**
	 * Sends the announcement message every 500ms
	 * @param {net.Socket} socket 
	 * @param {Number} port 
	 * @param {String} addr 
	 * @param {JSON} msg 
	 */
	announceMessage(socket, port, addr, msg) {
		if (this.running) {
			this.sendMessage(socket, port, addr, msg);
			setTimeout(() => this.announceMessage(socket, port, addr, msg), 500);
		}
	}

	/**
	 * sends msg to addr:port via socket
	 * @param {net.Socket} socket 
	 * @param {Number} port 
	 * @param {String} addr 
	 * @param {JSON} msg 
	 */
	sendMessage(socket, port, addr, msg) {
		let buff = Buffer.from(JSON.stringify(msg));
		socket.send(buff, 0, buff.length, port, addr, (err) => {
			if (err) {
				console.log(err);
				this.running = false;
			}
		});
	}

	/**
	 * generates a single AnnounceMessage
	 * @returns {JSON}
	 */
	generateAnnounceMessages() {
		return {
			command: {
				name: "announce",
				server: {
					ip: "123.123.123.123",
					port: PORT,
					id: uuidv4(),
					name: "FakeServer Do not connect!"
				}
			}
		};
	}

	/**
	 * connects to Ip:Port
	 * @param {String} ip 
	 * @param {Number} port
	 * @returns {Promise<void>} 
	 */
	async connect(ip, port) {
		return new Promise((resolve, reject) => {
			if (!port) {
				port = PORT;
			}
			this.socket = net.createConnection(port, ip, () => {
				this.connected = true;
				resolve();
			});
		});
	}

	/**
	 * sends all incoming messages of socket to the console
	 */
	logAllIncomingMessages() {
		if (this.socket && this.connected) {
			this.socket.on('data', (data) => {
				console.log(data.toString('utf8'));
			});
		}
	}

	/**
	 * sends data to the socket
	 * @param {String} data 
	 */
	async send(data) {
		if (this.socket && this.connected && data && data != "") {
			return new Promise((resolve,reject) => {
				this.socket.write(data,resolve,reject);
			});
		}
	}

	/**
	 * sends data with newline to the socket
	 * @param {String} data 
	 */
	async sendLn(data) {
		if (data && data != "") {
			await this.send(data + "\n");
		}
	}

	/**
	 * loads the state from states/filename.state
	 * @param {String} filename
	 * @returns {JSON}  
	 */
	loadState(filename) {
		if (!filename) {
			return null;
		}
		if (!/\.state/.test(filename)) {
			filename += ".state";
		}
		const path = "states/" + filename;
		if (fs.existsSync(path)) {
			return JSON.parse(fs.readFileSync(path, 'utf8'));
		}
	}

	/**
	 * saves the given state to states/filename.state
	 * if filename is undefined, the current timestamp will be used
	 * @param {JSON} state only the state, no action
	 * @param {String} filename Filename to store the State to (default: UnixTime)
	 */
	saveState(state, filename) {
		if (!filename) {
			filename = Math.floor(new Date() / 1000);
		}
		if (!/\.state/.test(filename)) {
			filename += ".state";
		}
		fs.writeFileSync("states/" + filename, JSON.stringify(state));
	}


	/**
	 * writes the next received GameState to disk.
	 * @param {String} filename Filename to store the State to (default: UnixTime) 
	 */
	writeNextGameState(filename) {
		if (this.socket && this.connected) {
			this.socket.on('data', (data) => {
				const cmd = data.toString('utf8');
				const state = JSON.parse(cmd);
				this.saveState(state.command, filename);
			});
		}
	}

	/**
	 * Loads the state from the disk and sends it to the server.
	 * @param {String} filename Filename to load the state from
	 */
	async sendGameState(filename) {
		const state = this.loadState(filename);
		if (state) {
			await this.sendLn({
				command: state
			});
		}
	}

	/**
	 * sends a Skip Message to the Server.
	 */
	async sendSkipMessage() {
		await this.sendLn(JSON.stringify({
			command: {
				name: "skip_turn",
			}
		}));
	}

	/**
	 * Sends a Join Message to the Server.
	 * @param {String} name Name of the Player
	 * @param {Number} col Color of the Player as Integer (Hex #RGBA)
	 * @param {Number} lost How often did the Player loose?
	 * @param {Number} won How often did the Player win?
	 */
	sendJoinMessage(name, col, lost, won) {
		let played = 0;
		if (lost && won) {
			played = lost + won;
		} else if (lost) {
			played = lost;
			won = 0;
		} else if (won) {
			played = won;
			lost = 0;
		}
		this.sendLn({
			command: {
				name: "join",
				profile: {
					name: name,
					id: uuidv4(),
					color: col,
					lost: lost,
					won: won,
					played: played,
					gender: 2
				}
			}
		});
	}

}