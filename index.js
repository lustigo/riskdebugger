const args = process.argv.slice(2);
const Debugger = require("./debugger");
const Proxy = require("./proxy");
let d = new Debugger();

function main() {
	const command = args[0];
	if (command == "announce") {
		announce();
	} else if (command == "logAnnounce") {
		logAnnounce();
	} else if (command == "help") {
		help();
		return;
	} else if (command == "watch") {
		connectAndLog();
	} else if (command == "loadstate") {
		connectAndLoadState();
	} else if (command == "savestate") {
		connectAndSaveState();
	} else if (command == "chat") {
		connectAndChat();
	} else if (command == "skip") {
		connectAndSkip();
	} else if (command == "proxy") {
		if (args.length < 4) {
			console.log("Not alle needed arguments passed.");
		} else {
			const a = getIpAndPort();
			let listeningPort;
			if (/\d{1,5}/.test(args[3])) {
				listeningPort = Number(args[3]);
			}
			if (!a.ip || !a.port || !listeningPort) {
				console.log("Not alle needed arguments passed.");
			} else {
				new Proxy(a.ip, a.port, listeningPort);
			}
		}
	} else {
		console.log("Command not found. Try node index help");
	}
}

function announce() {
	let amount = 1;
	if (args.length > 1) {
		if (/\d?/.test(args[1])) {
			amount = args[1];
		} else {
			console.log("Please enter as an argument a number");
		}
	}
	console.log("Sending " + amount + " Announcments...");
	console.log("Press CRTL+C to Stop");
	d.announce(amount);
}

function logAnnounce() {
	let number = 4;
	if (args.length > 1) {
		if (/\d/.test(args[1]) && args[1] == 6) {
			number = 6
		}
	}
	console.log("Logging Announcements for IPV" + number + " ...");
	console.log("Press CRTL+C to Stop");
	if (number == 4) {
		d.logAnnounce4();
	} else {
		d.logAnnounce6();
	}
}

async function connect(ip, port) {
	if (!ip) {
		ip = "127.0.0.1";
	}
	await d.connect(ip, port);
}

function getIpAndPort() {
	let ip;
	let port;
	if (args.length > 1) {
		ip = args[1];
	}
	if (args.length > 2) {
		if (/\d{1,5}/.test(args[2])) {
			port = args[2];
		}
	}
	return {
		ip: ip,
		port: Number(port)
	};
}

function getIpAndPortAndNameAndColor() {
	let ip;
	let port;
	let name;
	let color;
	if (args.length > 1) {
		name = args[1]
	}
	if (args.length > 2) {
		if (/-?\d+/.test(args[2])) {
			color = Number(args[2]);
		}
	}
	if (args.length > 3) {
		ip = args[3];
	}
	if (args.length > 4) {
		if (/\d{1,5}/.test(args[4])) {
			port = args[4];
		}
	}
	return {
		ip: ip,
		port: port,
		name: name,
		color: color
	};
}

async function connectAndLog() {
	let connectionDetails = getIpAndPort();
	await connect(connectionDetails.ip, connectionDetails.port);
	d.logAllIncomingMessages();
}

async function connectAndSkip() {
	let connectionDetails = getIpAndPort();
	await connect(connectionDetails.ip, connectionDetails.port);
	d.sendSkipMessage().then(() => process.exit(0)).catch(() => process.exit(1));
}

function getFileNameAndIpAndPort() {
	let ip;
	let port;
	let filename;
	if (args.length > 1 && /^[a-z]/.test(args[1])) {
		filename = args[1];
	} else if (args.length > 1 && /^[0-9]/.test(args[1])) {
		return getIpAndPort();
	}
	if (args.length > 2) {
		ip = args[2];
	}
	if (args.length > 3) {
		if (/\d{1,5}/.test(args[3])) {
			port = args[3];
		}
	}
	return {
		ip: ip,
		port: port,
		filename: filename
	};
}

async function connectAndSaveState() {
	let args = getFileNameAndIpAndPort();
	await connect(args.ip, args.port);
	d.writeNextGameState(args.filename);
}

async function connectAndLoadState() {
	let args = getFileNameAndIpAndPort();
	if (args.filename) {
		await connect(args.ip, args.port);
		d.sendGameState(args.filename).then(() => process.exit(0)).catch(() => process.exit(1));
	}
}

async function connectAndChat() {
	let args = getIpAndPortAndNameAndColor();
	if (!args.color) {
		args.color = 4278190080; //#FF000000
	}
	if (!args.name) {
		args.name = "Server";
	}
	await connect(args.ip, args.port);
	d.logChat();

	setInputListener(msg => d.sendChatMessage(msg, args.name, args.color));
}

function setInputListener(func) {
	var stdin = process.openStdin();

	stdin.addListener("data", function (d) {
		func(d.toString().trim());
	});
}


function help() {
	console.log("Usage: node index <COMMAND>");
	console.log("Commands:");
	console.log("\tannounce Send Fake Announcement Messages (amount: How many different default:1), every Message is sent via v4 and v6");
	console.log("\twatch <ip> <port> (optional, default: localhost, 7777) Logs all recieved Messages to the console.");
	console.log("\tsavestate <filename> <ip> <port>(optional, default: localhost, 7777) Writes the next received gamestate to disk.");
	console.log("\tloadstate <filename> <ip> <port> (optional, default: localhost, 7777) Loads the given state from disk and sets it on the server.");
	console.log("\tlogAnnounce <4/6 DEFAULT: 4> log (new) Announcement Messages");
	console.log("\tchat <name> <color> <ip> <port> (optional, default: localhost, 7777)");
	console.log("\tskip <ip> <port> (optional, default: localhost, 7777) Connects to the Server and sends a Skip Message");
	console.log("\tproxy <serverIp> <serverPort> <listeningIp> NOT OPTIONAL, Connects to Server and starts listening on the listeningIp. Redirects all incoming messages.");
	console.log("\thelp Shows this help message");
}

main();