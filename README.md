# Risk Debugger
## Usage
1. Set Environment Variable RISKDEBUG=1
2. Start the Server via GUI
3. Start the Debugger `node index <METHOD> <OptionalArgument>`
4. Start the Game

## Methods
- announce
  - sends fake Announcement Messages (argument how many different) (no debug mode needed, no start of the server needed)
- logAnnounce
  - logs all (new) Announcement Messages (no debug mode needed, no start of the server needed)
- watch
  - connects to the Server, prints all messages that are broadcasted to all players (no debug mode needed)
- savestate
  - connects to the Server, waits until the next state is broadcasted, saves it in a file (in the states directory) (no debug mode needed)
- loadState
  - connects to the Server, tries to set the given gameState
- chat
  - connects to the Server, prints all received chat messages and gives you an interactive shell to send some

## Use as a Library
You can use this Debugger as a Library (e.g. in the interactive Node shell) with `require("./debugger.js")`