const net = require("net");

module.exports = class Proxy {

	/**
	 * init the Proxy, connect to Server and start the listening
	 * @param {String} serverIp
	 * @param {Number} serverPort
	 * @param {Number} listeningPort
	 */
	constructor(serverIp, serverPort, listeningPort) {
		this.serverIp = serverIp;
		this.serverPort = serverPort;
		this.listeningPort = listeningPort;
		this.connect();
		this.startListening();
	}

	/**
	 * connect to Server
	 */
	connect() {
		this.toServer = net.createConnection(this.serverPort, this.serverIp);
		this.toServer.on('data', (data) => this.onServerData(data));
	}

	/**
	 * start internal server
	 */
	startListening() {
		this.serverSocket = net.createServer((socket) => {
			this.toClient = socket;
			this.toClient.on('data', (data) => this.onClientData(data));
		}).listen(this.listeningPort);
	}

	/**
	 * on Data from Server
	 * @param {Buffer} data
	 */
	onServerData(data) {
		this.sendToClient(this.manipulateToClient(JSON.parse(data.toString('utf-8'))));
	}

	/**
	 * on Data from Client
	 * @param {Buffer} data
	 */
	onClientData(data) {
		this.sendToServer(this.manipulateToServer(JSON.parse(data.toString('utf-8'))));
	}

	/**
	 * manipulate Packets coming from the Server, going to the Client
	 * @param {JSON} data
	 * @returns {JSON} manipulated data
	 */
	manipulateToClient(data) {
		return data;
	}

	/**
	 * manipulate Packets coming from the Client, going to the Server
	 * @param {JSON} data
	 * @returns {JSON} manipulated data
	 */
	manipulateToServer(data) {
		return data;
	}

	/**
	 * Sends data to Client
	 * @param {String} data 
	 */
	sendToClient(data) {
		this.toClient.write(JSON.stringify(data));
	}


	/**
	 * Sends data to Server
	 * @param {String} data 
	 */
	sendToServer(data) {
		this.toServer.write(JSON.stringify(data));
	}
}